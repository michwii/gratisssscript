#! /bin/bash
cd
sudo apt-get install openjdk-7-jdk -y
wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins-ci.org/debian binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins -y
#On ajoute l'user jenkins dans le fichier sudoer (ca lui permet d'appeller la commande sudo sans avoir a mettre de password
sudo su
sudo echo "jenkins ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers 
#ATTENTION il semblerait que la ligne du dessus ne marche pas en mode auto mais que en mode manuel...
exit
#Maintenant on restart le server
sudo /etc/init.d/./jenkins restart